mod handlers;

use crate::handlers::search_movie;
use axum::{routing::get, Router};
use dotenvy::dotenv;
use std::net::SocketAddr;

#[tokio::main]
async fn main() {
  dotenv().ok();

  let search_routes = Router::new().route("/movie", get(search_movie));
  let api_v1_routes = Router::new().nest("/search", search_routes);
  let app = Router::new().nest("/api/v1", api_v1_routes);
  let addr = SocketAddr::from(([127, 0, 0, 1], 3000));

  axum::Server::bind(&addr).serve(app.into_make_service()).await.unwrap();
}
