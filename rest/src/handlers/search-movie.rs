use axum::{extract::Query, http::StatusCode, response::IntoResponse, Json};
use serde::{Deserialize, Serialize};
use shared::services::tmdb;

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct SearchMovieQuery {
  query: String,
}

pub async fn search_movie(query: Query<SearchMovieQuery>) -> impl IntoResponse {
  let res = tmdb::search_movie(query.query.clone()).await;
  (StatusCode::OK, Json(res))
}
