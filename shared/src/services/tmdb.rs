use crate::structs::Movie;
use serde::{Deserialize, Serialize};
use std::env;

#[derive(Deserialize, Serialize, Debug, Clone, Default)]
pub struct TmdbSearchMovieResponse {
  pub page: usize,
  pub results: Vec<Movie>,
}

pub async fn search_movie(query: String) -> Vec<Movie> {
  let tmdb_api_key = env::var("TMDB_API_KEY").unwrap();

  let res = reqwest::get(format!(
    "https://api.themoviedb.org/3/search/movie?api_key={}&query={}",
    tmdb_api_key, query
  ))
  .await
  .unwrap()
  .json::<TmdbSearchMovieResponse>()
  .await
  .unwrap();

  res.results
}

#[cfg(test)]
mod tests {
  use super::*;
  use dotenvy::dotenv;

  fn setup() {
    dotenv().ok();
  }

  #[tokio::test]
  async fn it_works() {
    setup();
    search_movie("Harry Potter".to_string()).await;
  }
}
