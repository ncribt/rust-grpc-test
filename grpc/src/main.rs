mod movies;

use dotenvy::dotenv;
use std::net::SocketAddr;
use tonic::transport::Server;

pub mod proto {
  tonic::include_proto!("movies");
}

#[tokio::main]
async fn main() {
  dotenv().ok();

  let grpc = proto::movies_server::MoviesServer::new(movies::Movies::default());
  let addr = SocketAddr::from(([127, 0, 0, 1], 3001));

  Server::builder().add_service(grpc).serve(addr).await.unwrap();
}
