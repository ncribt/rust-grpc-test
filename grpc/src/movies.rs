use crate::proto;
use shared::services::tmdb;
use tonic::{Request, Response, Status};

#[derive(Debug, Default)]
pub struct Movies {}

#[tonic::async_trait]
impl proto::movies_server::Movies for Movies {
  async fn search_movie(
    &self,
    request: Request<proto::SearchMovieRequest>,
  ) -> Result<Response<proto::SearchMovieResponse>, Status> {
    let res = tmdb::search_movie(request.into_inner().query).await;
    Ok(Response::new(proto::SearchMovieResponse {
      movies: res
        .into_iter()
        .map(|m| proto::Movie { title: m.title })
        .collect(),
    }))
  }
}
